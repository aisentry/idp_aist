from django.shortcuts import render, redirect 
from django.http import HttpResponse
from django.forms import inlineformset_factory
from django.contrib.auth.forms import UserCreationForm

from django.contrib.auth import authenticate, login, logout

from django.contrib import messages

from django.contrib.auth.decorators import login_required

# Create your views here.
from .models import *
from .forms import *
from detection.models import *



def registerPage(request):
	if request.user.is_authenticated:
		return redirect('home')
	else:
		form = CreateUserForm()
		if request.method == 'POST':
			form = CreateUserForm(request.POST)
			if form.is_valid():
				form.save()
				user = form.cleaned_data.get('username')
				messages.success(request, 'Account was created for ' + user)

				return redirect('login')
			

		context = {'form':form}
		return render(request, 'accounts/register.html', context)

def loginPage(request):
	if request.user.is_authenticated:
		return redirect('home')
	else:
		if request.method == 'POST':
			username = request.POST.get('username')
			password =request.POST.get('password')

			user = authenticate(request, username=username, password=password)

			if user is not None:
				login(request, user)
				if request.user.is_staff:
					return redirect('admin_page')
				else:
					return redirect('user_page')
			else:
				messages.info(request, 'Username OR password is incorrect')

		context = {}
		return render(request, 'accounts/login.html', context)

def logoutUser(request):
	logout(request)
	return redirect('login')



def home(request):
	context={}
	return render(request, 'accounts/dashboard.html', context)

def admin_page(request):
	context = {}
	return render(request, 'accounts/admin_page.html', context)

def user_page(request):
	context = {}
	return render(request, 'accounts/user_page.html', context)

def report_data(request):
	context = {}
	return render(request, 'accounts/report_data.html', context)

def photos(request):
	photos = Detection.objects.all()
	return render(request, 'accounts/photos.html', {'photos' : photos})

def log_report(request):
	logs = Detection.objects.all()
	return render(request, 'accounts/log_report.html', {'logs' : logs})
	

